# Contributions des membres d'e-Planets.

Ce dépot liste les publications et autres contributions scientifiques des
membres d'e-Planets sous forme de fichiers au format bibtex.

## Fichiers individuels

Chaque membre est encouragé à maintenir la liste de ses
publications/contributions dont il est auteur principal, ou des collaborations
dont il est auteur secondaire.

Chacun peut créer un répertoire dans le sous-dossier `bib` et y organiser
sa bibliographie à sa guise: fichier unique, fichiers par années, etc.

Si l'auteur principal est dans le groupe et vous voulez ajouter la référence
pour lui/elle, faites le dans son fichier pour éviter les duplicatas.

## Biblio de groupe

Pour concatener tous les sous-fichiers bib en une biblio unique :

```sh
$ make eplanets.bib
```

Pour créer un document bibliographique (demande d'avoir une installation
LaTeX) :

```sh
$ make eplanets.pdf
```
