.PHONY: eplanets.bib

eplanets.bib:
	cat $$(find bib -name '*.bib') > $@

eplanets.pdf: eplanets.bib
	latexmk -xelatex -use-make eplanets.tex
